from hyperopt import fmin, tpe, Trials

from widedeep.data.airbnb import AirbnbSettings
from widedeep.evaluation.random_search import RandomSearch
import widedeep.utils as ut
from widedeep.evaluation.search_utils import run_network

import os
import tensorflow as tf

logger = ut.get_logger('data')

flags = tf.app.flags
FLAGS = flags.FLAGS

# Performance parameters - Not to be modified
flags.DEFINE_float("gpu_frac", 0.90, "Percentage of GPU memory to use")
flags.DEFINE_integer("workers", 2, "Workers to use for dequeing")
flags.DEFINE_integer("mem_deq", 2, "Memory factor (~GB) to use for dequeing")
flags.DEFINE_integer("summaries", 1000, "Steps between summaries")

# Validation parameters
flags.DEFINE_integer("patience", 4, "Consecutive allowed loss increases in early stopping")
flags.DEFINE_integer("validate_steps", 100, "Batch size to use for validation")
flags.DEFINE_integer("validate_interval", 5000, "Batch size to use for validation")
flags.DEFINE_integer("trials", 32, "Number of random trials")

# Data parameters
tf.app.flags.DEFINE_bool('use_price', True, 'Whether to predict price (True) or availability (False)')
flags.DEFINE_float('classification', False, 'Whether to build classification or regression dataset')
flags.DEFINE_bool("data_augm", True, "Whether to randomly crop images or not")
bins = None
settings = AirbnbSettings

# CNN Network to use - it is a constant so far
flags.DEFINE_string("cnn_network", ut.NetworkModels.ALEXNET, "Network to use for the CNN")

# Models to use
flags.DEFINE_bool("linear", False, "Whether to use a linear model")
flags.DEFINE_bool("mlp", True, "Whether to use a deep model")
flags.DEFINE_bool("cnn", False, "Whether to use a CNN model")


def parameters_identity_str():
    """ Return a string formatted identifier of the experiment """
    # Create string with the identity of the model
    bins_str = '' if bins is None else '_'.join(bins)
    models = '_'.join(["linear", str(FLAGS.linear), "mlp", str(FLAGS.mlp), "cnn", str(FLAGS.cnn)])
    target = 'price' if FLAGS.use_price else 'available'
    return '_'.join(['airbnb',
                     target,
                     bins_str,
                     models,
                     'data_augm' + str(FLAGS.data_augm),
                     'classif' + str(FLAGS.classification),
                     'patience' + str(FLAGS.patience),
                     'validate_int' + str(FLAGS.validate_interval),
                     'network' + str(FLAGS.cnn_network),
                     ])


def run(params):
    """ Run early stop on the network according to parameters """

    logger.warn("Launching trial for {}".format(params))

    # Create subfolder within default output
    folder = os.path.join(ut.get_default_output(), parameters_identity_str())

    # Select dataset
    dataset = ut.Datasets.AIRBNB_PRICE if FLAGS.use_price else ut.Datasets.AIRBNB_AVAILABLE

    return run_network(logs_root=folder,
                       settings=settings,
                       location=ut.get_dataset_location(dataset),
                       data_augm=FLAGS.data_augm,
                       is_classification=FLAGS.classification,
                       params=params,
                       patience=FLAGS.patience,
                       validate_int=FLAGS.validate_interval,
                       validate_steps=FLAGS.validate_steps,
                       summaries=FLAGS.summaries,
                       workers=FLAGS.workers,
                       mem_deq=FLAGS.mem_deq,
                       gpu_frac=FLAGS.gpu_frac,
                       bins=None,
                       cnn_net=FLAGS.cnn_network)


trials = Trials()

# Create search space and perform trials
rs = RandomSearch(linear_model=FLAGS.linear, mlp_model=FLAGS.mlp, cnn_model=FLAGS.cnn)
best = fmin(fn=run, space=rs.get_search_space(), algo=tpe.suggest, max_evals=FLAGS.trials, trials=trials)

# Store trials as pickle
trials_path = os.path.join(ut.get_default_output(), parameters_identity_str() + '.trials')
ut.save_pickle(trials_path, trials)

# Store best as pickle
best_path = os.path.join(ut.get_default_output(), parameters_identity_str() + '.best')
ut.save_pickle(best_path, best)

# Show best
logger.info("Best is {}".format(best))

logger.info('Showing trials...')
for i, trial in enumerate(trials.trials):
    logger.info("Trial {}: {}".format(i, trial))

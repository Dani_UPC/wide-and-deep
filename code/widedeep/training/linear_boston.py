# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Example widedeep for TensorFlow Wide & Deep Tutorial using TF.Learn API."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from widedeep.data.boston import COLUMN_NAMES, LABEL_NAME
from widedeep.data.data_ops import split_data
from sklearn.datasets import load_boston
import tempfile
import numpy as np

flags = tf.app.flags
FLAGS = flags.FLAGS

# Regularization
flags.DEFINE_string("l2_reg", 0.1, "L2 regularization coefficient.")  # Tend to make gradient stay around zero
flags.DEFINE_string("l1_reg", 0.1, "L1 regularization coefficient.")  # Tend to make gradients be zero

flags.DEFINE_string("lr", 0.1, "Learning rate.")

flags.DEFINE_string("model_dir", "/home/walle/workspace/pfm/logs_deep", "Base directory for output models.")
flags.DEFINE_string("model_type", "wide", "Valid model types: {'wide', 'deep', 'wide_n_deep'}.")
flags.DEFINE_integer("train_steps", 200, "Number of training steps.")
flags.DEFINE_string("train_data", "", "Path to the training data.")
flags.DEFINE_string("test_data", "", "Path to the test data.")


def build_estimator(model_dir):
    """Build an estimator."""

    wide_columns = [tf.contrib.layers.real_valued_column(c) for c in COLUMN_NAMES]

    # TODO could create some cross columns even

    '''age_buckets = tf.contrib.layers.bucketized_column(age,
                                                      boundaries=[
                                                          18, 25, 30, 35, 40, 45,
                                                          50, 55, 60, 65
                                                      ])
    tf.contrib.layers.crossed_column([education, occupation],
                                                     hash_bucket_size=int(1e4))'''

    if FLAGS.model_type == "wide":
        print('Training wide model ...')

        # Linear classifier with different options for optimization
        # https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/learn/python/learn/estimators/linear.py

        # Regularization + Learning rate
        lr = None if FLAGS.lr is None else FLAGS.lr

        m = tf.contrib.learn.LinearRegressor(feature_columns=wide_columns,
                                              optimizer=tf.train.FtrlOptimizer(learning_rate=lr),
                                              model_dir=model_dir)
        '''m = tf.contrib.learn.DNNLinearCombinedRegressor(
            model_dir=model_dir,
            linear_feature_columns=wide_columns,
            enable_centered_bias=False)'''

    elif FLAGS.model_type == "deep":
        print('Training deep model ...')
        # TODO: add regularization here. Default model is Adagrad
        m = tf.contrib.learn.DNNRegressor(model_dir=model_dir,
                                           feature_columns=wide_columns,
                                           hidden_units=[100, 50])
    else:
        print('Training wide and deep model ...')
        # Combination of both models, where each model has its own optimizer and loss function
        # Outputs are summed
        m = tf.contrib.learn.DNNLinearCombinedRegressor(
            model_dir=model_dir,
            linear_feature_columns=wide_columns,
            dnn_feature_columns=wide_columns,
            dnn_hidden_units=[100, 50],
            enable_centered_bias=False)

    return m


def input_fn(data, labels):
    """ Input builder function. Returns a dictionary for the features and the label column """
    continuous_cols = {c: tf.constant(data[:, i]) for i, c in enumerate(COLUMN_NAMES)}
    # Converts the label column into a constant Tensor.
    label = tf.constant(labels)
    # Returns the feature columns and the label
    return continuous_cols, label


def train_and_eval():
    """Train and evaluate the model."""

    # Read boston data
    boston = load_boston()
    features, labels = np.array(boston.data), np.array(boston.target)

    # Separate between training and validation
    train_inds, test_inds = split_data(features.shape[0], train_ratio=0.80)
    train_data, train_labels = features[train_inds], labels[train_inds]
    test_data, test_labels = features[test_inds], labels[test_inds]

    '''df_train[LABEL_COLUMN] = (
        df_train["income_bracket"].apply(lambda x: ">50K" in x)).astype(int)
    df_test[LABEL_COLUMN] = (
        df_test["income_bracket"].apply(lambda x: ">50K" in x)).astype(int)'''

    model_dir = tempfile.mkdtemp() if not FLAGS.model_dir else FLAGS.model_dir
    print("model directory = %s" % model_dir)

    m = build_estimator(model_dir)
    x, y = input_fn(train_data, train_labels)
    m.fit(input_fn=lambda: input_fn(train_data, train_labels), steps=FLAGS.train_steps, batch_size=128)
    results = m.evaluate(input_fn=lambda: input_fn(test_data, test_labels), steps=1)
    a = m.predict(input_fn=lambda: input_fn(test_data, test_labels))
    print('Mean from test prediction: {}. Std from test prediction: {}'.format(np.mean(a), np.std(a)))
    print('Absolute error from test {}'.format(np.mean(np.abs(test_labels - a))))
    print('Test results: \n')
    for key in sorted(results):
        print("%s: %s" % (key, results[key]))


def main(_):
    train_and_eval()


if __name__ == "__main__":
    tf.app.run()

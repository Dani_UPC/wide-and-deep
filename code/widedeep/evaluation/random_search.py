import hyperopt.pyll.stochastic
from hyperopt import hp

from widedeep.ops.losses import Optimizers
from widedeep.utils import NetworkWeights


class RandomSearch(object):

    """ Class that tracks the best validation error on a number of trials using
    random search of the search space, proven to be more computationally efficient than
    grid search
    """

    def __init__(self, linear_model, mlp_model, cnn_model, is_mdn=True):
        """
        Args:
            linear_model: Whether sample search for a linear model
            mlp_model: Whether to sample search for  MLP model (deep)
            cnn_model: Whether to sample search for a CNN model
            is_mdn: Whether the model to train is a Mixture Density Network or a traditional one
        """
        self.linear_model = linear_model
        self.mlp_model = mlp_model
        self.cnn_model = cnn_model
        self.mdn = is_mdn

    def _define_search_space(self):
        """ Defines the parameter search space for the networks """

        # General parameters
        space = {
            'batch_size': hp.choice('batch_size', [32, 64]),
            # Embedding dimensions as power of two, so it is 2^embedding_power2 which lies in u(2, 5)
            'embeddings': hp.uniform('embeddings', 2, 5),
            # Exponent to L1 regularization so it is 10^-l1_log_exp
            'l1_log_exp': hp.choice('l1_reg', [None, hp.uniform('l1_log_exp', -3, -5)]),
            # Exponent to L2 regularization so it is 10^-l1_log_exp
            'l2_log_exp': hp.choice('l2_reg', [None, hp.uniform('l2_log_exp', -3, -5)]),
            # Gradient norm clip, so it is in 2^gradient_log_clip which lies in u(2, 7)
            'gradient_log_clip': hp.choice('norm_clip', [None, hp.uniform(2, 7)),
            # Whether network is MDN or not
        }

        # If MDN, add number of mixtures as u(3, 8)
        space.update({'K': hp.uniform('K', 3, 8)})

        # Add linear model if requested
        if self.linear_model:
            space.update(self.linear_search_space())

        # Add mlp model if requested
        if self.mlp_model:
            space.update(self.mlp_search_space())

        # Add cnn model if requested
        if self.cnn_model:
            space.update(self.cnn_search_space())

        return space

    def sample(self):
        """ Returns a dictionary containing a sample of the parameters """
        space = self._define_search_space()
        space_params = hyperopt.pyll.stochastic.sample(space)
        # Add MDN information
        space_params['mdn'] = self.mdn
        return space_params

    def get_search_space(self):
        return self._define_search_space()

    @staticmethod
    def linear_prefix():
        return 'linear'

    @staticmethod
    def mlp_prefix():
        return 'mlp'

    @staticmethod
    def cnn_prefix():
        return 'cnn'

    @staticmethod
    def linear_field(name):
        return RandomSearch.label(RandomSearch.linear_prefix(), name)

    @staticmethod
    def mlp_field(name):
        return RandomSearch.label(RandomSearch.mlp_prefix(), name)

    @staticmethod
    def cnn_field(name):
        return RandomSearch.label(RandomSearch.cnn_prefix(), name)

    @staticmethod
    def _linear_opts():
        return [Optimizers.Ftrl, Optimizers.SGD]

    @staticmethod
    def _mlp_opts():
        return [Optimizers.Momentum, Optimizers.SGD]

    @staticmethod
    def _cnn_opts():
        return [Optimizers.Momentum, Optimizers.SGD]

    @staticmethod
    def label(prefix, name):
        return '_'.join([prefix, name])

    def _shared_parameter_space(self, prefix, optimizers):
        """ Given a model label and their optimizers returns the common parameters search space """
        return {

            # Exponent to learning rate so it is 10^-lr_log_exp
            self.label(prefix, 'lr_log_exp'): hp.uniform(self.label(prefix, 'lr_log'), -2, -5),

            # LR decay steps to be between ~ u(20000, 80000)
            self.label(prefix, 'lr_decay_steps'):
                hp.quniform(self.label(prefix, 'lr_decay_steps'), 20000, 80000, 1),

            # LR decay rate to be between ~ u(0.1, 0.75)
            self.label(prefix, 'lr_decay_rate'):
                hp.uniform(self.label(prefix, 'lr_decay_rate'), 0.1, 0.75),

            self.label(prefix, 'optimizer'):
                hp.choice(self.label(prefix, 'optimizer'), optimizers),
        }

    def linear_search_space(self):
        """ Specific linear search space """
        # Linear space has no extra parameter space
        return self._shared_parameter_space(self.linear_prefix(), self._linear_opts())

    def mlp_search_space(self):
        """ Specific MLP search space """
        general_space = self._shared_parameter_space(self.mlp_prefix(), self._mlp_opts())

        mlp_space = {

            # Add hidden units as exp of 10^hidden_log_exp, where hidden_log_exp is u(2, 4)
            self.label(self.mlp_prefix(), 'hidden_log_exp'):
                hp.uniform(self.label(self.mlp_prefix(), 'hidden_log_exp'), 2, 4),

            # Add number of layers, that is in u(1, 4)
            self.label(self.mlp_prefix(), 'hidden_layers'):
                hp.quniform(self.label(self.mlp_prefix(), 'hidden_layers'), 1, 4, 1),

            # Dropout, to be in u(0.0, 0.5)
            self.label(self.mlp_prefix(), 'dropout'):
                hp.uniform(self.label(self.mlp_prefix(), 'dropout'), 0.0, 0.5)

            # Batch norm, to be in [True, False]
            self.label(self.mlp_prefix(), 'batch_norm'):
                hp.choice(self.label(self.mlp_prefix(), 'batch_norm'), [True, False])
        }

        # Join model parameters settings with MLP parameters
        general_space.update(mlp_space)
        return general_space

    def cnn_search_space(self):
        """ Specific CNN search space """
        general_space = self._shared_parameter_space(self.cnn_prefix(), self._cnn_opts())

        cnn_space = {

            # Weights for warm start
            self.label(self.cnn_prefix(), 'weights'):
                hp.choice(self.label(self.cnn_prefix(), 'weights'),
                          [NetworkWeights.IMAGENET, NetworkWeights.PLACES_365, None]),

            # Dropout
            self.label(self.cnn_prefix(), 'dropout'):
                hp.uniform(self.label(self.mlp_prefix(), 'dropout'), 0.0, 0.5)
        }

        # Join model parameters settings with MLP parameters
        general_space.update(cnn_space)
        return general_space


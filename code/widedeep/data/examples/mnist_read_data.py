"""

Reads the Proto files containing the MNIST dataset. Expects several files to be containd in the
directory, identified by the set of data they belong to and the slice of data they represent.

For a summary of the columns see mnist_build_data.py.

"""

from widedeep.data.reading_ops import DataReader
from widedeep.data.mnist import MnistSettings
from widedeep.data.data_ops import DataMode
from widedeep.data.image_ops import get_image_specs
import widedeep.utils as ut

import scipy.misc
import tensorflow as tf

logger = ut.get_logger('data')

tf.app.flags.DEFINE_string('batch_size', 10,
                           'Number of instances per batch')
tf.app.flags.DEFINE_integer('memory_factor', 1,
                            'Factor related to the capacity of the queue (~GB). The '
                            + ' higher this amount, the more mixed the data but the slower '
                            + 'the processing time as well ')
tf.app.flags.DEFINE_integer('reader_threads', 8,
                            'Number of threads to read the instances.')
FLAGS = tf.app.flags.FLAGS


if __name__ == '__main__':

    with tf.Session() as sess:

        # MNIST settings
        dataset = MnistSettings(dataset_location=ut.get_dataset_location(ut.Datasets.MNIST),
                                image_specs=get_image_specs(ut.NetworkModels.MNIST,
                                                               batch_size=FLAGS.batch_size,
                                                               mean=[0.0, 0.0, 0.0],
                                                               random_crop=False))

        # Read batches from dataset
        reader = DataReader(dataset)
        features, label = reader.read_batch(batch_size=FLAGS.batch_size,
                                            data_mode=DataMode.TRAINING,  # Whiehever works here, e.g. training
                                            memory_factor=FLAGS.memory_factor,
                                            reader_threads=FLAGS.reader_threads,
                                            train_mode=False)

        # Initi all vars
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        # Define coordinator to handle all threads
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord, sess=sess)

        example, l = sess.run([features, label])

        # Print first instance in batch
        print(example)
        # Save image to check it is correct
        scipy.misc.imsave('test.jpeg', example['image'][5])
        # Show price for instance
        print('Label: ' + str(l[5]))

        coord.request_stop()
        coord.join(threads)

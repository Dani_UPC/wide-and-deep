"""

Reads the Proto files containing the Boston. Expects several files to be contained in the
directory, identified by the set of data they belong to and the slice of data they represent.

For a summary of the columns see boston_build_data.py.

"""

from widedeep.data.scikit_dataset import BostonSettings
from widedeep.data.reading_ops import DataReader
from widedeep.data.data_ops import DataMode
import widedeep.utils as ut

import tensorflow as tf

logger = ut.get_logger('data')

tf.app.flags.DEFINE_string('batch_size', 5, 'Number of instances per batch')
tf.app.flags.DEFINE_integer('memory_factor', 2,
                            'Factor related to the capacity of the queue (~GB). The '
                            + ' higher this amount, the more mixed the data but the slower '
                            + 'the processing time as well ')
tf.app.flags.DEFINE_integer('reader_threads', 2,
                            'Number of threads to read the instances.')

FLAGS = tf.app.flags.FLAGS


if __name__ == '__main__':

    with tf.Session() as sess:

        # Boston settings
        data_name = ut.Datasets.BOSTON
        dataset = BostonSettings(dataset_location=ut.get_dataset_location(data_name))

        logger.info("Loading %s data" % data_name)

        # Read batches from dataset
        reader = DataReader(dataset)
        features, label = reader.read_batch(batch_size=FLAGS.batch_size,
                                            data_mode=DataMode.TRAINING, # Use whatever here, e.g. training
                                            memory_factor=FLAGS.memory_factor,
                                            reader_threads=FLAGS.reader_threads,
                                            train_mode=False)

        # Initi all vars
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        # Define coordinator to handle all threads
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord, sess=sess)

        example, l = sess.run([features, label])

        # Print first instance in batch
        print(example)
        # Show price for instance
        print 'Targets: ' + str(l)

        coord.request_stop()
        coord.join(threads)

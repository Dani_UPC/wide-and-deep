# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Example widedeep for TensorFlow Wide & Deep Tutorial using TF.Learn API."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tempfile
from six.moves import urllib
import pandas as pd
import tensorflow as tf

flags = tf.app.flags
FLAGS = flags.FLAGS

# Regularization
flags.DEFINE_string("l2_reg", 0.1, "L2 regularization coefficient.")  # Tend to make gradient stay around zero
flags.DEFINE_string("l1_reg", 0.1, "L1 regularization coefficient.")  # Tend to make gradients be zero

flags.DEFINE_string("lr", 0.1, "Learning rate.")

flags.DEFINE_string("model_dir", "", "Base directory for output models.")
flags.DEFINE_string("model_type", "deep",
                    "Valid model types: {'wide', 'deep', 'wide_n_deep'}.")
flags.DEFINE_integer("train_steps", 200, "Number of training steps.")
flags.DEFINE_string(
    "train_data",
    "",
    "Path to the training data.")
flags.DEFINE_string(
    "test_data",
    "",
    "Path to the test data.")

COLUMNS = ["age", "workclass", "fnlwgt", "education", "education_num",
           "marital_status", "occupation", "relationship", "race", "gender",
           "capital_gain", "capital_loss", "hours_per_week", "native_country",
           "income_bracket"]
LABEL_COLUMN = "label"
CATEGORICAL_COLUMNS = ["workclass", "education", "marital_status", "occupation",
                       "relationship", "race", "gender", "native_country"]
CONTINUOUS_COLUMNS = ["age", "education_num", "capital_gain", "capital_loss",
                      "hours_per_week"]


def maybe_download():
    """May be downloads training data and returns train and test file names."""
    if FLAGS.train_data:
        train_file_name = FLAGS.train_data
    else:
        train_file = tempfile.NamedTemporaryFile(delete=False)
        urllib.request.urlretrieve("https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data",
                                   train_file.name)  # pylint: disable=line-too-long
        train_file_name = train_file.name
        train_file.close()
        print("Training data is downloaded to %s" % train_file_name)

    if FLAGS.test_data:
        test_file_name = FLAGS.test_data
    else:
        test_file = tempfile.NamedTemporaryFile(delete=False)
        urllib.request.urlretrieve("https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.test",
                                   test_file.name)  # pylint: disable=line-too-long
        test_file_name = test_file.name
        test_file.close()
        print("Test data is downloaded to %s" % test_file_name)

    return train_file_name, test_file_name


def build_estimator(model_dir):
    """Build an estimator."""
    # Define categorical columns
    gender = tf.contrib.layers.sparse_column_with_keys(column_name="gender",
                                                       keys=["female", "male"])
    race = tf.contrib.layers.sparse_column_with_keys(column_name="race",
                                                     keys=["Amer-Indian-Eskimo",
                                                           "Asian-Pac-Islander",
                                                           "Black", "Other",
                                                           "White"])
    # This type of columns are string columns from which we dont have the complete
    # mapping between strings and ids
    education = tf.contrib.layers.sparse_column_with_hash_bucket(
        "education", hash_bucket_size=1000)
    marital_status = tf.contrib.layers.sparse_column_with_hash_bucket(
        "marital_status", hash_bucket_size=100)
    relationship = tf.contrib.layers.sparse_column_with_hash_bucket(
        "relationship", hash_bucket_size=100)
    workclass = tf.contrib.layers.sparse_column_with_hash_bucket(
        "workclass", hash_bucket_size=100)
    occupation = tf.contrib.layers.sparse_column_with_hash_bucket(
        "occupation", hash_bucket_size=1000)
    native_country = tf.contrib.layers.sparse_column_with_hash_bucket(
        "native_country", hash_bucket_size=1000)

    # Continuous base columns.
    age = tf.contrib.layers.real_valued_column("age")
    education_num = tf.contrib.layers.real_valued_column("education")
    capital_gain = tf.contrib.layers.real_valued_column("capital_gain")
    capital_loss = tf.contrib.layers.real_valued_column("capital_loss")
    hours_per_week = tf.contrib.layers.real_valued_column("hours_per_week")

    # Transformations -> Binning of a real valued column into specified bins
    age_buckets = tf.contrib.layers.bucketized_column(age,
                                                      boundaries=[
                                                          18, 25, 30, 35, 40, 45,
                                                          50, 55, 60, 65
                                                      ])

    # Wide columns: categorical columns + cross transformations
    # Crossed columns to be seen as a the hash of the
    # cartesian product between columns of the input columns
    wide_columns = [gender, native_country, education, occupation, workclass,
                    marital_status, relationship, age_buckets,
                    tf.contrib.layers.crossed_column([education, occupation],
                                                     hash_bucket_size=int(1e4)),
                    tf.contrib.layers.crossed_column(
                        [age_buckets, race, occupation],
                        hash_bucket_size=int(1e6)),
                    tf.contrib.layers.crossed_column([native_country, occupation],
                                                     hash_bucket_size=int(1e4))]

    # Deep columns
    deep_columns = [
        tf.contrib.layers.embedding_column(workclass, dimension=8),
        tf.contrib.layers.embedding_column(education, dimension=8),
        tf.contrib.layers.embedding_column(marital_status,
                                           dimension=8),
        tf.contrib.layers.embedding_column(gender, dimension=8),
        tf.contrib.layers.embedding_column(relationship, dimension=8),
        tf.contrib.layers.embedding_column(race, dimension=8),
        tf.contrib.layers.embedding_column(native_country,
                                           dimension=8),
        tf.contrib.layers.embedding_column(occupation, dimension=8),
        age,
        education_num,
        capital_gain,
        capital_loss,
        hours_per_week,
    ]

    if FLAGS.model_type == "wide":
        print('Training wide model ...')

        # Linear classifier with different options for optimization
        # https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/learn/python/learn/estimators/linear.py

        # Regularization + Learning rate
        l1 = 0.0 if FLAGS.l1_reg is None else FLAGS.l1_reg
        l2 = 0.0 if FLAGS.l2_reg is None else FLAGS.l2_reg
        lr = None if FLAGS.lr is None else FLAGS.lr

        m = tf.contrib.learn.LinearClassifier(feature_columns=[
            gender, native_country, education, occupation, workclass, marital_status, race,
            age_buckets],
            optimizer=tf.train.FtrlOptimizer(
                learning_rate=lr,
                l1_regularization_strength=l1,
                l2_regularization_strength=l2),
            model_dir=model_dir)

    elif FLAGS.model_type == "deep":
        print('Training deep model ...')
        # TODO: add regularization here. Default model is Adagrad
        # Deep Neural Network classifier
        # Fully connected layer using the specified units
        # https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/learn/python/learn/estimators/dnn.py
        m = tf.contrib.learn.DNNClassifier(model_dir=model_dir,
                                           feature_columns=deep_columns,
                                           hidden_units=[100, 50])
    else:
        print('Training wide and deep model ...')
        # TODO: add regularization here. Default model is Adagrad
        # Combination of both models
        # Each model has its own optimizer and loss function
        # Output of each model is summed
        m = tf.contrib.learn.DNNLinearCombinedClassifier(
            model_dir=model_dir,
            linear_feature_columns=wide_columns,
            dnn_feature_columns=deep_columns,
            dnn_hidden_units=[100, 50],
            enable_centered_bias=False)
    return m


def input_fn(df):

    """Input builder function.
        - Creates constant Tensors for continuous columns
        - Creates Sparse Tensors for categorical columns
    Creates a dictionary so each column name is a key and the value is the tensor """
    # Creates a dictionary mapping from each continuous feature column name (k) to
    # the values of that column stored in a constant Tensor.
    #continuous_cols = {k: tf.constant(df[k].values) for k in CONTINUOUS_COLUMNS}
    continuous_cols = {k: tf.constant(df[k].values[0:100]) for k in CONTINUOUS_COLUMNS}

    # Creates a dictionary mapping from each categorical feature column name (k)
    # to the values of that column stored in a tf.SparseTensor.
    # Single column -> [i, 0]
    # Refer to https://www.tensorflow.org/versions/r0.10/api_docs/python/sparse_ops.html#SparseTensor
    # for more info
    categorical_cols = {k: tf.SparseTensor(
        #indices=[[i, 0] for i in range(df[k].size)],
        indices=[[i, 0] for i in range(100)],
        #values=df[k].values,
        values=df[k].values[0:100],
        #shape=[df[k].size, 1])
        shape=[100, 1])
                        for k in CATEGORICAL_COLUMNS}

    print(categorical_cols['occupation'])

    # Merges the two dictionaries into one.
    feature_cols = dict(continuous_cols)
    feature_cols.update(categorical_cols)
    # Converts the label column into a constant Tensor.
    #label = tf.constant(df[LABEL_COLUMN].values)
    label = tf.constant(df[LABEL_COLUMN].values[0:100], shape=(100,1))
    # Returns the feature columns and the label.
    return feature_cols, label


def train_and_eval():
    """Train and evaluate the model."""
    train_file_name, test_file_name = maybe_download()
    df_train = pd.read_csv(train_file_name,
        names=COLUMNS,
        skipinitialspace=True)
    df_test = pd.read_csv(test_file_name,
        names=COLUMNS,
        skipinitialspace=True,
        skiprows=1)

    df_train[LABEL_COLUMN] = (
        df_train["income_bracket"].apply(lambda x: ">50K" in x)).astype(int)
    df_test[LABEL_COLUMN] = (
        df_test["income_bracket"].apply(lambda x: ">50K" in x)).astype(int)

    model_dir = tempfile.mkdtemp() if not FLAGS.model_dir else FLAGS.model_dir
    print("model directory = %s" % model_dir)

    m = build_estimator(model_dir)
    m.fit(input_fn=lambda: input_fn(df_train), steps=FLAGS.train_steps)
    results = m.evaluate(input_fn=lambda: input_fn(df_test), steps=1)
    print('Test results: \n')
    for key in sorted(results):
        print("%s: %s" % (key, results[key]))


def main(_):
    train_and_eval()


if __name__ == "__main__":
    tf.app.run()

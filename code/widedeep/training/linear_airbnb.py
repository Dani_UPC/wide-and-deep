# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Example widedeep for TensorFlow Wide & Deep Tutorial using TF.Learn API."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from widedeep.data.airbnb import AirbnbSettings
from widedeep.data.data_ops import TrainMode
from widedeep.data.image_ops import get_image_specs
from widedeep.model.joint_model import *
from widedeep.utils import *


flags = tf.app.flags
FLAGS = flags.FLAGS

# Regularization
flags.DEFINE_integer("classes", 1, "Number of classes in Airbnb")
flags.DEFINE_integer("batch_size", 64, "Batch size to use.")
flags.DEFINE_string("network", NetworkModels.ALEXNET, "Network to use")
flags.DEFINE_integer("summaries", 100, "Steps between summaries.")
flags.DEFINE_integer("checkpoints", 500, "Steps between model checkpoints.")
flags.DEFINE_integer("steps", 2500, "Steps to train.")
flags.DEFINE_float("gpu_frac", 0.70, "Percentage of GPU memory to use.")
flags.DEFINE_string("mode", TrainMode.WIDE, "Architecture to use")
flags.DEFINE_bool("training", True, "Execution mode")
flags.DEFINE_bool("log_placement", False, "Whether to log variable device placement")

# CNN parameters
flags.DEFINE_float("cnn_initial_lr", 0.01, "Initial learning rate for the cnn model.")
flags.DEFINE_integer("cnn_decay_steps", 10000, "Steps at which learning rate decreases for the cnn model.")
flags.DEFINE_float("cnn_decay_rate", 0.5, "Decrease rate of the learning rate for the cnn model.")

# Linear parameters
flags.DEFINE_float("linear_initial_lr", 0.2, "Initial learning rate for the linear model.")
flags.DEFINE_integer("linear_decay_steps", 25000, "Steps at which learning rate decreases for the linear model.")
flags.DEFINE_float("linear_decay_rate", 0.5, "Decrease rate of the learning rate for the linear model.")


def read_data(reader, is_training):
    return reader.read_batch(FLAGS.batch_size, is_training, 2, 4)


def train_and_eval():
    """Train and evaluate the model."""

    # Select Airbnb dataset
    dataset = AirbnbSettings(dataset_location=get_dataset_location(Datasets.AIRBNB),
                             image_specs=get_image_specs(FLAGS.network,
                                                         batch_size=FLAGS.batch_size))

    # Define columns
    wide = dataset.get_wide_columns(excluded_cols=['path', 'price', 'height', 'id', 'room_id',
                                                   'format', 'host_id', 'colorspace'])
    image = dataset.get_image_column()

    # Get data
    reader = DataReader(dataset)

    # Fit linear model
    m =tf.contrib.learn.DNNLinearCombinedRegressor(model_dir=get_default_output(),
            linear_feature_columns=wide)
    m.fit(input_fn=lambda: read_data(reader, True), steps=FLAGS.steps)

    # Evaluate
    results = m.evaluate(input_fn=lambda: read_data(reader, False), steps=1)

    print('Test results: \n')
    for key in sorted(results):
        print("%s: %s" % (key, results[key]))


def main(_):
    train_and_eval()


if __name__ == "__main__":
    tf.app.run()

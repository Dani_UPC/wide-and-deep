"""

Airbnb representing data gathered from Airbnb platform. Data was tracked between 07/17/2015 and 12/08/2016
 in different important cities. For further information check 'airbnb_build_data.py' and 'airbnb_read_data.py'

Total of ~300k instances.

"""

import widedeep.data.data_ops as do
from widedeep.data.serialization_ops import SerializeSettings
from widedeep.data.reading_ops import DataSettings
from widedeep.utils import get_logger, load_pickle

import pandas as pd
import numpy as np
import tensorflow as tf


logger = get_logger('data')


#### Airbnb serialization

# TODO: download pictures in notebooks so links are already loaded

METADATA_COLUMNS = ['listing_url', 'last_scraped', 'id', 'scrape_id', 'picture_url']
PRICE_COLUMN = 'final_price'
AVAILABILITY_COLUMN = 'availability_365'


class AirbnbSerialize(SerializeSettings):

    def __init__(self, data_path, amenities_path, nq=5, subset=None, predict_price=True):
        """
        Args:
            data_path: Path where dataset is stored
            amenities_path: Path containing list of amenities
            nq: Number of quantiles for numeric data normalization
            subset: Number of instances to use for the data. Use None to include all
            predict_price: Whether to use price as the prediction column (True) or the availability (False).
                Availability will be normalized into interval [0,1].
        """
        super(AirbnbSerialize, self).__init__(data_path)
        self.amenities_list = list(load_pickle(amenities_path)['amenities'])
        self.nq = nq
        self.num_instances = None
        self.edges, self.categories = {}, {}
        self.data = None
        self.subset = subset
        self.target = PRICE_COLUMN if predict_price else AVAILABILITY_COLUMN

        # This is a list of hardcoded bucket sizes for sparse columns.
        # If name not found here, the set of keys is extracted from the data
        self.hardcoded_buckets = {}


    def read(self):
        # Read data with pandas
        self.data = pd.read_csv(self.data_path)
        self.num_instances = self.data.shape[0]

        # Sort columns by name so the order is always the same
        self.data = self.data.reindex_axis(sorted(self.data.columns), axis=1)

        # Normalize availability into 0 and 1 when it is a target
        if self.target == AVAILABILITY_COLUMN:
            self.data[AVAILABILITY_COLUMN] = self.data[AVAILABILITY_COLUMN]/365.0

        # Track set of categories for each categorical column
        for (name, dtype) in do.get_column_info(self.data, excluded=METADATA_COLUMNS):
            if do.is_categorical(dtype):
                logger.info("Tracking values in categorical column %s" % name)
                self.categories[name] = self.data[name].unique().tolist()

        # Define hardcoded buckets
        max_data = self.data.shape[0]
        self.hardcoded_buckets = {'last_scraped': max_data, 'picture_url': max_data, 'listing_url': max_data}


    def get_validation_indices(self, train_ratio, val_ratio):
        # Select subset of data if requested
        num = self.num_instances if self.subset is None else self.subset
        train, val, test = do.split_data(num, train_ratio, val_ratio)
        # Determine indices for training, validation and testing
        train_ind = self.data.index.values[train]
        val_ind = self.data.index.values[val]
        test_ind = self.data.index.values[test]
        # Normalize using training data (training + validation)
        self.data, self.edges = do.normalize_data(self.data,
                                                  train_ind=np.concatenate([train_ind, val_ind]),
                                                  zscores=self.nq is None,
                                                  nq=self.nq,
                                                  excluded=METADATA_COLUMNS + [self.target])
        return train_ind, val_ind, test_ind


    def get_options(self):
        options = {
            'data_path': self.data_path,
            'norm_metadata': self.edges,
            'amenities': self.amenities_list,
            'categories': self.categories,
            'target': self.target,
            'format': 'JPEG'
        }
        return options


    def build_examples(self, index):
        row, amenities, url = self.get_row(index=index)
        try:
            # If more than one instance wanted to be generated for a single row, here is where we add them
            return [self._build_example(row, amenities, url)]
        except Exception as e:
            logger.error('Could not build instance from index {}: {}'.format(index, e.message))
            return []


    def define_columns(self):
        """ See base class """
        columns = []
        for (name, ftype) in do.get_column_info(self.data, excluded=self.amenities_list + ['picture_url']):

            if do.is_categorical(ftype):
                # Categorical data

                if name in self.hardcoded_buckets:
                    # If hardcoded, take value
                    keys = self.hardcoded_buckets[name]
                else:
                    # Otherwise is safe to take it from the dataset itself
                    keys = self.categories[name]

                logger.info('Creating column for feature "{}" with keys "{}"'.format(name, keys))
                columns.append(do.SparseColumn(name,
                                               keys=keys,
                                               type=do.map_feature_type(self.data[name].dtype)))

            elif do.is_numeric(ftype) or do.is_bool(ftype):
                logger.info('Creating column for numerical feature "{}"'.format(name))
                columns.append(do.NumericColumn(name,
                                                type=do.map_feature_type(self.data[name].dtype)))

            else:
                raise RuntimeError('Unknown column type "{}" for column "{}"'.format(name, ftype))

        # Add amenities column
        am_keys = self.hardcoded_buckets['amenities'] if 'amenities' in self.hardcoded_buckets \
            else self.amenities_list
        logger.info('Creating column for "amenities" with keys {}'.format(am_keys))
        columns.append(do.SparseColumn('amenities',
                                       keys=am_keys,
                                       type=do.map_feature_type(np.dtype('object'))))

        # Image-specific columns
        columns += [
            do.ImageColumn('image', format='JPEG'),
            do.NumericColumn('height', type=do.map_feature_type(np.dtype('int'))),
            do.NumericColumn('width', type=do.map_feature_type(np.dtype('int'))),
            do.SparseColumn('path', do.map_feature_type(np.dtype('object'))),
            do.SparseColumn('format', do.map_feature_type(np.dtype('object'))),
            do.SparseColumn('colorspace', do.map_feature_type(np.dtype('object')))
        ]

        for c in columns:
            logger.info('Creating column for feature "{}"'.format(c.name))

        return columns


    def _build_example(self, row, amenities, url):
        """ Builds an example by building a map of Proto features.
        Args:
            row: Dictionary with row information and type
            amenities: List of amenities of the example
            url: URL of the image to include
        """
        # Map features into corresponding types
        feature_dict = {name: do.map_feature(row[name]['value'], row[name]['type'])
                        for (name, info) in row.iteritems()}

        # Create Sparse Column for the amenities of the row
        feature_dict.update({'amenities': do.bytes_feature(amenities)})

        # Load image from url
        decoded_image, height, width = self.image_from_url(url, jpeg=True)

        # Add image metadata
        feature_dict.update({
            'image': do.bytes_feature(decoded_image),
            'height': do.int64_feature(height),
            'width': do.int64_feature(width),
            'path': do.bytes_feature(url),
            'format': do.bytes_feature('JPEG'),  # Following standard from Alexnet example
            'colorspace': do.bytes_feature('RGB')  # Following standard from Alexnet example
        })

        # Build example
        return tf.train.Example(features=tf.train.Features(feature=feature_dict))


    def get_row(self, index):
        """ Returns row information given the room identifier """
        # Get instance and copy into dict
        ref_row = self.data.ix[index].to_dict()
        # Get base features
        feature_info = do.get_column_info(self.data, excluded=['picture_url'] + self.amenities_list)
        row = {name: {'value': ref_row[name], 'type': ftype} for (name, ftype) in feature_info}
        # Get image path
        img_url = ref_row['picture_url']
        # Get amenities
        amenities = [am for am in self.amenities_list if ref_row[am]]
        return row, amenities, img_url


#### Airbnb batching


class AirbnbSettings(DataSettings):

    def __init__(self, dataset_location, image_specs=None, embedding_dimensions=32, quantizer=None):
        """ Airbnb read settings.
        Args:
            bins: Edges to use for quantizing the price column. More in data_ops.py.
        """
        super(AirbnbSettings, self).__init__(dataset_location=dataset_location,
                                             image_specs=image_specs,
                                             embedding_dimensions=embedding_dimensions,
                                             quantizer=quantizer)


    def size_per_instance(self):
        return 1


    def target_class(self):
        return self.serialize_options['target']


    def _target_type(self):
        """ For both availability and price the target column is real valued """
        return tf.float32


    def _get_num_classes(self):
        """ Since they are real-valued targets, there is a single output """
        return 1


    def _metadata_columns(self):
        """ Returns the set of metadata columns to be excluded for training """
        return METADATA_COLUMNS + ['path', 'height', 'width', 'format', 'colorspace']


    def select_wide_cols(self):
        columns = []
        for (k, v) in list(self.columns.iteritems()):

            if k not in self._metadata_columns():

                if isinstance(v, do.SparseColumn) or isinstance(v, do.NumericColumn):
                    columns.append(v.to_column())

        return columns
        '''
        # Bucketize numeric columns
        acc_buckets = self.columns['accommodates'].categorize(args=[2, 4, 6])
        bathroom_buckets = self.columns['bathrooms'].categorize(args=[1, 3])
        bedrooms_buckets = self.columns['bedrooms'].categorize(args=[2, 4, 6])
        guests_buckets = self.columns['guests_included'].categorize(args=[1, 2, 4, 8])
        price_buckets = self.columns['final_price'].categorize(args=[50, 100, 150, 250])

        # Create column as cartesian product of both sets
        cross1 = [acc_buckets, bathroom_buckets, bedrooms_buckets, guests_buckets, price_buckets,
                  self.columns['property_type'].to_column()]
        cross2 = [self.columns['area'].to_column(), self.columns['subarea'].to_column()]

        result = []
        for c1 in cross1:
            for c2 in cross2:
                result.append(create_cross_column([c1, c2]))
        return result
        '''


    def select_deep_cols(self):
        columns = []
        for (k, v) in list(self.columns.iteritems()):

            if k not in self._metadata_columns():

                if isinstance(v, do.SparseColumn):
                    columns.append(v.to_embedding(dims=self.embedding_dims))

                elif isinstance(v, do.NumericColumn):
                    columns.append(v.to_column())

        return columns

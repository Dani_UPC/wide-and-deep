from widedeep.data.airbnb import AirbnbSettings
import widedeep.data.data_ops as do
import widedeep.data.quantize as qu
from widedeep.model.model_base import CNNModel, LinearModel, MLP
from widedeep.data.image_ops import get_image_specs
from widedeep.model.joint_model import JointRegressor, JointClassifier
from widedeep.ops.losses import Optimizers, CrossEntropy, MeanSquared
import widedeep.ops.metrics as me
import widedeep.utils as ut

import tensorflow as tf

logger = ut.get_logger('data')

flags = tf.app.flags
FLAGS = flags.FLAGS


flags.DEFINE_integer("batch_size", 64, "Batch size to use")
flags.DEFINE_integer("validate_steps", 100, "Batch size to use for validation")
flags.DEFINE_integer("validate_interval", 3000, "Batch size to use for validation")

flags.DEFINE_integer("summaries", 100, "Steps between summaries")
flags.DEFINE_integer("checkpoints", 5000000, "Steps between model checkpoints")

flags.DEFINE_integer("steps", 200000, "Steps to train")
flags.DEFINE_float("gpu_frac", 0.90, "Percentage of GPU memory to use")
flags.DEFINE_integer("workers", 2, "Workers to use for dequeing")
flags.DEFINE_integer("mem_deq", 2, "Memory factor (~GB) to use for dequeing")
flags.DEFINE_string("mode", do.TrainMode.DEEP, "Architecture to use")

flags.DEFINE_bool("training", True, "Execution mode")
flags.DEFINE_bool("validate", True, "Whether to use validation for training")
flags.DEFINE_integer("patience", 3, "Consecutive allowed loss increases in early stopping")

tf.app.flags.DEFINE_bool('use_price', True, 'Whether to predict price (True) or availability (False)')
flags.DEFINE_float('classification', True, 'Whether to build classification or regression dataset')

# Regularization
flags.DEFINE_float("l1_regularization", None, "L1 regularization for the loss. Set to None to disable")
flags.DEFINE_float("l2_regularization", None, "L2 regularization for the loss. Set to None to disable")

# Gradient 
flags.DEFINE_float("gradient_clip", None, "If not None, value to use for clipping the gradient")

# CNN parameters
flags.DEFINE_bool("data_augm", True, "Whether to randomly crop images or not")
flags.DEFINE_float("cnn_initial_lr", 1e-5, "Initial learning rate for the cnn model")
flags.DEFINE_integer("cnn_decay_steps", 100000, "Steps at which learning rate decreases for the cnn model")
flags.DEFINE_float("cnn_decay_rate", 0.1, "Decrease rate of the learning rate for the cnn model")
flags.DEFINE_string("cnn_network", ut.NetworkModels.ALEXNET, "Network to use for the CNN")
flags.DEFINE_string("cnn_weights", ut.NetworkWeights.PLACES_365, "Weights to use for the network")

# Linear parameters
flags.DEFINE_float("linear_initial_lr", 1e-1, "Initial learning rate for the linear model")
flags.DEFINE_integer("linear_decay_steps", 80000, "Steps at which learning rate decreases for the linear model")
flags.DEFINE_float("linear_decay_rate", 0.1, "Decrease rate of the learning rate for the linear model")

# MLP parameters
flags.DEFINE_float("mlp_initial_lr", 1e-1, "Initial learning rate for the MLP model")
flags.DEFINE_integer("mlp_decay_steps", 80000, "Steps at which learning rate decreases for the MLP model")
flags.DEFINE_float("mlp_decay_rate", 0.1, "Decrease rate of the learning rate for the MLP model")
flags.DEFINE_integer("embeddings", 8, "Number of dimensions for the embedding columns")
flags.DEFINE_string("mlp_network", ut.NetworkModels.MLP, "Network to use for the MLP")


if __name__ == '__main__':

    # Select Airbnb dataset
    data = ut.Datasets.AIRBNB_PRICE if FLAGS.use_price else ut.Datasets.AIRBNB_AVAILABLE
    bins = [50, 120, 180, 300] if FLAGS.classification else None # prices for 5000
    # bins = [80, 150, 300] if FLAGS.classification else None # prices for 5000
    # bins = [65, 105.54, 191.16] if FLAGS.classification else None # prices for 10000
    # bins = [0.5589] if FLAGS.classification else None # availability normalized for 10000
    # bins = [0.249315, 0.66849] if FLAGS.classification else None  # availability normalized for ~300k, with 3 classes: low, normal, high
    # bins = [0.5041] if FLAGS.classification else None # availability normalized for ~300k
    # bins = [95] if FLAGS.classification else None # price for ~300k
    dataset = AirbnbSettings(dataset_location=ut.get_dataset_location(data),
                             image_specs=get_image_specs(FLAGS.cnn_network,
                                                         batch_size=FLAGS.batch_size,
                                                         random_crop=FLAGS.data_augm),
                             embedding_dimensions=FLAGS.embeddings,
                             quantizer=qu.Quantize(edges=bins, batch_size=FLAGS.batch_size))

    # Define columns
    wide, deep, image = dataset.get_wide_columns(), dataset.get_deep_columns(), dataset.get_image_column()

    # Show columns
    print('Using wide columns: {}'.format([x.name for x in wide]))
    print('Using deep columns: {}'.format([x.name for x in deep]))
    print('Using image columns: {}'.format([x.name for x in image]))

    # Build CNN for image analysis
    cnn_weights = ut.get_model_weights(FLAGS.cnn_network, FLAGS.cnn_weights) \
        if FLAGS.cnn_weights is not None else None
    cnn_model = CNNModel('cnn',
                         columns_list=image,
                         num_classes=dataset.get_num_classes(),
                         cnn_layers=ut.get_network_definition(FLAGS.cnn_network),
                         optimizer=Optimizers.RMS,
                         initial_lr=FLAGS.cnn_initial_lr,
                         decay_steps=FLAGS.cnn_decay_steps,
                         decay_rate=FLAGS.cnn_decay_rate,
                         weights=cnn_weights,
                         excluded_layers=[])
                         #excluded_layers=['fc8', 'fc7', 'fc6'])

    # Build linear model for memorization
    linear_model = LinearModel('linear',
                               columns_list=wide,
                               num_classes=dataset.get_num_classes(),
                               optimizer=Optimizers.Ftrl,
                               initial_lr=FLAGS.linear_initial_lr,
                               decay_steps=FLAGS.linear_decay_steps,
                               decay_rate=FLAGS.linear_decay_rate)

    # Build MLP for generalization
    mlp_model = MLP('mlp',
                    columns_list=deep,
                    num_classes=dataset.get_num_classes(),
                    layers=ut.get_network_definition(FLAGS.mlp_network),
                    optimizer=Optimizers.Momentum,
                    initial_lr=FLAGS.mlp_initial_lr,
                    decay_steps=FLAGS.mlp_decay_steps,
                    decay_rate=FLAGS.mlp_decay_rate)

    # Set models according to settings
    if FLAGS.mode == do.TrainMode.WIDE:
        models = [linear_model]
    elif FLAGS.mode == do.TrainMode.DEEP:
        models = [mlp_model]
    elif FLAGS.mode == do.TrainMode.WIDE_AND_DEEP:
        models = [linear_model, mlp_model]
    elif FLAGS.mode == do.TrainMode.CNN:
        models = [cnn_model]
    elif FLAGS.mode == do.TrainMode.ALL:
        models = [cnn_model, linear_model, mlp_model]


    # Define model, loss and metrics according to model type
    if FLAGS.classification:
        logger.info('Using classification problem for column {} with bins: {}'
                    .format(dataset.target_class(), bins))
        model_f, loss_f = JointClassifier, CrossEntropy
        metrics = [me.Accuracy(), me.AccuracyMode(dataset.get_num_classes()),
                   me.AccuracyRandom(dataset.get_num_classes()),
                   me.ConfusionMatrix(num_classes=dataset.get_num_classes())]
    else:
        logger.info('Regression problem for column %s' % dataset.target_class())
        model_f, loss_f = JointRegressor, MeanSquared
        metrics = [me.AbsError(), me.MeanPredError(), me.MedianPredError(), me.Mean(),
                   me.Std(), me.AbsErrorInterval(cuts=[50, 100, 150, 250])]

    # Build model
    joint = model_f(ut.get_default_output(),
                    models=models,
                    l1_reg=FLAGS.l1_regularization,
                    l2_reg=FLAGS.l2_regularization,
                    loss_fn=loss_f(),
                    clip_gradient=FLAGS.gradient_clip)

    if FLAGS.training:

        if FLAGS.validate:
            # Train using validation
            joint.train_and_validate(dataset=dataset,
                                     batch_size=FLAGS.batch_size,
                                     validate_interval=FLAGS.validate_interval,
                                     validate_steps=FLAGS.validate_steps,
                                     track_summaries=FLAGS.summaries,
                                     steps=FLAGS.steps,
                                     gpu_frac=FLAGS.gpu_frac,
                                     metrics=metrics,
                                     patience=FLAGS.patience,
                                     workers=FLAGS.workers,
                                     mem_dequeing=FLAGS.mem_deq)

        else:
            # Train without validation
            joint.train(dataset,
                        FLAGS.batch_size,
                        track_models=FLAGS.checkpoints,
                        track_summaries=FLAGS.summaries,
                        steps=FLAGS.steps,
                        gpu_frac=FLAGS.gpu_frac,
                        metrics=metrics,
                        workers=FLAGS.workers,
                        mem_dequeing=FLAGS.mem_deq)
    else:
        # Evaluate validation
        results = joint.evaluate(dataset,
                                 batch_size=FLAGS.batch_size,
                                 data_mode=do.DataMode.TEST,
                                 track_summaries=FLAGS.summaries,
                                 gpu_frac=FLAGS.gpu_frac,
                                 metrics=metrics,
                                 workers=FLAGS.workers,
                                 mem_dequeing=FLAGS.mem_deq)
        logger.info(results)


# ! /usr/bin/python

import pickle
import time
import os
import logging.config
import logging
import json
import urllib
import requests
import re

# TODO: Define several loggers


class Datasets:
    AIRBNB_PRICE = 'airbnb_price'
    AIRBNB_AVAILABLE = 'airbnb_availability'
    MNIST = 'mnist'
    BOSTON = 'boston'
    DIABETES = 'diabetes'


class NetworkModels:
    ALEXNET = 'alexnet'
    VGG = 'vgg'
    MNIST = 'mnist'
    MLP = 'mlp'
    OWN = 'own'


class NetworkWeights:
    PLACES_365 = 'places_365'
    IMAGENET = 'imagenet'


# Key folders
CODE_ROOT = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.join(os.path.join(CODE_ROOT, '..'), '..')
DATA_ROOT = os.path.join(ROOT_DIR, 'data')

# Airbnb data folder
AIRBNB_ROOT = os.path.join(DATA_ROOT, 'airbnb')
AIRBNB_GENERAL = os.path.join(AIRBNB_ROOT, 'general.csv')
AIRBNB_FINAL = os.path.join(AIRBNB_ROOT, 'final.csv')
AIRBNB_METADATA = os.path.join(AIRBNB_ROOT, 'metadata.dat')

# Places365 models paths
PLACES_MODEL_FOLDER = os.path.join(DATA_ROOT, 'models')

# Repository of external weight storage
WEIGHTS_REPO = 'https://bitbucket.org/Dani_UPC/wide-and-deep-resources/raw/master'

# Categories for places 365
PLACES_CATEGORIES = os.path.join(DATA_ROOT, 'categories_places365.txt')

# Places predictions for Alexnet (code in places.py)
PLACES_PREDICTIONS_FOLDER = os.path.join(DATA_ROOT, 'places_predictions')

# Mnist data
MNIST_DATA = os.path.join(DATA_ROOT, 'mnist')

# Datasets folder
DATASETS_FOLDER = os.path.join(DATA_ROOT, 'datasets')

# Default model output
MODEL_FOLDER = os.path.join(ROOT_DIR, 'logs')

LOG_CONFIG = os.path.join(os.path.join(CODE_ROOT, 'logs'), 'logging.conf')
LOG_OUT = os.path.join(os.path.join(CODE_ROOT, 'logs'), 'wide_deep.log')


logger = None


def get_places_categories():
    return PLACES_CATEGORIES


def get_model_weights(network, weights):
    """ Returns the path to the weights for the given model. If they do not exist,
    they are downloaded from the corresponding repository """
    weight_path = os.path.join(os.path.join(PLACES_MODEL_FOLDER, network), weights + '.npy')
    if not os.path.isfile(weight_path):
        logger.info("Weights not found. Trying to download them ...")
        download_weights(network, weights, weight_path)
    return weight_path


def download_weights(network, weights, path):
    """ Downloads the weights from the external repository given the weight identifier """
    url = '/'.join([WEIGHTS_REPO, network, weights + '.npy'])
    logger.info("Downloading weights {} from {} into {}".format(weights, url, path))
    download_file(url, path)


def get_places_predictions(name='places_3.data'):
    return os.path.join(PLACES_PREDICTIONS_FOLDER, name)


def get_network_definition(network_tag):
    return os.path.join(os.path.join(PLACES_MODEL_FOLDER, network_tag), network_tag + '.config')


def get_dataset_location(name):
    return os.path.join(DATASETS_FOLDER, name)


def dataset_exists(name):
    return os.path.exists(get_dataset_location(name))


def get_default_output():
    return MODEL_FOLDER


def load_pickle(path):
    """ Loads object from a pickle file """
    with open(path, 'rb') as f:
        content = pickle.load(f)
    return content


def save_pickle(path, obj):
    """ Loads object from a pickle file """
    with open(path, 'wb') as f:
        pickle.dump(obj, f)


def opt_save_pickle(path, obj):
    """ Saves the object as pickle if path is not None """
    if path is not None:
        save_pickle(path, obj)


def read_json(path):
    """ Reads metadata from JSON file and dumps it into memory.
    If file is too memory consuming, consider using json for json streaming. """
    with open(path, 'r') as f:
        data = json.load(f)
    return data


def save_json(path, obj):
    with open(path, 'wb') as f:
        json.dump(obj, f)


def opt_save_json(path, obj):
    """ Saves the object as pickle if path is not None """
    if path is not None:
        save_json(path, obj)


def current_time_id():
    """ Returns the current time ID in milliseconds """
    return int(round(time.time() * 1000))


def create_dir(path):
    """ Creates directory if it does not exist """
    if not os.path.exists(path):
        os.makedirs(path)


def get_parent(path):
    """ Returns the parent folder of the given path """
    return os.path.abspath(os.path.join(path, os.pardir))


def download(url, dst):
    """ Downloads the content in the given url. If path provided, downloads into that path """
    urllib.request.urlretrieve(url, dst)


def get_filename_url(url):
    """
    Args:
        url: Input url
    Returns:
        Returns the corresponding file name and its extension
    """
    name = os.path.basename(url)
    return os.path.splitext(name)


def has_method(obj, method):
    """ Returns whether input object contains the given method """
    method_op = getattr(obj, method, None)
    return callable(method_op)


def initialize_logging(name):
    """ Initializes logging with the default configuration and the
    output where to store the logs. Set to None to disable storing into file
     :param name: Name of the logger to initialize """
    logging.config.fileConfig(LOG_CONFIG, defaults={'logfilename': LOG_OUT})
    logger = logging.getLogger(name)
    logger.propagate = False
    return logger


def get_logger(name):
    """ Returns logger instance """
    global logger
    if logger is None:
        logger = initialize_logging(name)
    return logger


logger = get_logger('data')


def get_parent_folder(folder):
    return os.path.abspath(os.path.join(folder, os.pardir))


def get_subfolders(folder):
    """ Returns the folders contained in the input path """
    return [name for name in os.listdir(folder) if os.path.isdir(os.path.join(folder, name))]


def download_file(file_url, path):
    """ Downloads a file into the respective path """
    urllib.urlretrieve(file_url, path)


def get_photos_urls(room_id):
    """ Returns the tuple of (id, url, caption) of the images for the input lodging """
    # Get HTML content
    url = "http://www.airbnb.com/rooms/" + str(room_id)
    content = requests.get(url)

    # Extract JSON from specific section
    pattern = '<script type="application/json" data-hypernova-key="p3hero_and_slideshowbundlejs">' + \
              '<!--(.*)--></script>'

    # Get only relevant fields
    matches = re.findall(pattern, content.text)

    if len(matches) > 1:
        raise RuntimeError('Found more than one match for room %s' % str(room_id))
    elif len(matches) == 0:
        raise ResourceNotFound('Could not find metadata for room %s' % (str(room_id)))

    # Get only metadata related to images
    images = json.loads(matches[0])['heroProps']['photos']
    return [(i['id'], i['picture'], i['caption']) for i in images]


def process_categories(cat_path):
    """ Returns the mapping between the identifier of a category in Places365 and its corresponding name
    Args:
        cat_path: Path containing the information about the Places365 categories
    """
    result = {}
    with open(cat_path) as f:
        lines = f.readlines()
    # Map each position to the corresponding category
    for l in lines:
        parts = l.split(' ')
        raw_cat, num_cat = parts[0].rstrip(), int(parts[1].rstrip())
        category = raw_cat[3:]  # Erase prefix related to first letter
        result[num_cat] = category
    return result


class ModelNotFound(ValueError):
    pass


class VariableNotFound(AttributeError):
    pass


class MultipleFound(AttributeError):
    pass


class FileNotFound(AttributeError):
    pass


class TypeError(ValueError):
    pass


class ResourceNotFound(ValueError):
    pass

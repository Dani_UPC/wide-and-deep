from hyperopt import STATUS_OK, STATUS_FAIL

from widedeep.data.image_ops import get_image_specs
from widedeep.model.model_base import MLP, CNNModel, LinearModel
from widedeep.evaluation.random_search import RandomSearch
from widedeep.model.joint_model import JointClassifier, JointRegressor, JointMDN
from widedeep.model.network_conf import create_mlp, create_alexnet
from widedeep.ops.losses import CrossEntropy, MeanSquared
import widedeep.ops.metrics as me
import widedeep.utils as ut

import numpy as np
import tempfile
import os

logger = ut.get_logger('data')


def run_network(logs_root, settings, location, data_augm, is_classification, params,
                patience, validate_int, validate_steps, summaries, workers=2, mem_deq=1,
                gpu_frac=0.80, quantizer=None, cnn_net=None):
    """ Performs an early stop procedure on a network given the input parameters.
    Networks to use depend on the parameters defined.
    Args:
        logs_root: Root folder where to store logs
        settings: Dataset settings for particular data.
        location: Location of the dataset.
        data_augm: Whether to use random cropping for image extraction.
        is_classification: Whether we deal with a classification problem or a regression one.
            If the model in the parameters is a Mixture Density Network, this is ignored.
        params: Hyperopt search space.
        patience: Consecutive validation evaluations without improvement permitted.
        validate_int: Steps between validation error computation.
        validate_steps: Number of batches to evaluate for validation.
        summaries: Steps between stats checkpoints.
        workers: Workers to use for data batching.
        mem_deq: Memory (~GB) reserved for reading data.
        gpu_frac: Fraction of the gpu to use.
        quantizer: Quantizer to categorize the target column. Set to None to disable it.
        cnn_net: If dataset uses image, this is the name of the network to use.
            Set to None for ignoring it.
    """

    if params['mdn']:
        is_classification = False

    # Create directory for current execution
    logs_path = os.path.join(logs_root, str(ut.current_time_id()))
    ut.create_dir(logs_path)

    # Store params in file
    params_file = os.path.join(logs_path, 'parameters.dat')
    ut.save_pickle(params_file, params)

    # Optional image specs
    image_specs = get_image_specs(cnn_net,
                                  batch_size=params['batch_size'],
                                  random_crop=data_augm) if cnn_net else None

    # Create dataset
    dataset = settings(dataset_location=location,
                       image_specs=image_specs,
                       quantizer=quantizer,
                       embedding_dims=read_embeddings(params))
    classes = dataset.get_num_classes()

    # Get dataset columns
    wide = dataset.get_wide_columns()
    deep = dataset.get_deep_columns()
    image = dataset.get_image_column()

    # Check mlp provided
    if RandomSearch.mlp_field('optimizer') in params:
        mlp_model, fd_mlp, mlp_path = read_mlp('mlp', cols=deep, classes=classes, params=params)
        mlp_model = [mlp_model]
    else:
        mlp_model = []

    # Check linear model provided
    if RandomSearch.linear_field('optimizer') in params:
        linear_model = [read_linear('linear', cols=wide, classes=classes, params=params)]
    else:
        linear_model = []

    # Check cnn model provided
    if RandomSearch.cnn_field('optimizer') in params:
        cnn_model, fd_cnn, cnn_path = read_cnn('cnn',
                                               cols=image,
                                               classes=classes,
                                               cnn_net=cnn_net,
                                               params=params)
    else:
        cnn_model = []

    # Build model
    joint, metrics = build_model(params, classes, is_classification)

    # Launch early stop
    try:
        steps, loss = joint.train_and_validate(dataset=dataset,
                                               batch_size=params['batch_size'],
                                               validate_interval=validate_int,
                                               validate_steps=validate_steps,
                                               track_summaries=summaries,
                                               gpu_frac=gpu_frac,
                                               metrics=metrics,
                                               patience=patience,
                                               workers=workers,
                                               mem_dequeing=mem_deq,
                                               keep_models=False)
    except Exception as e:
        logger.error('Exception found %s' % e.message)
        return {'loss': np.inf, 'status': STATUS_FAIL}

    # Check mlp provided
    if RandomSearch.mlp_field('optimizer') in params:
        os.close(fd_mlp)
        os.remove(mlp_path)

    if RandomSearch.cnn_field('optimizer') in params:
        os.close(fd_cnn)
        os.remove(cnn_path)

    return {'loss': loss, 'status': STATUS_OK, 'steps': steps}


def build_model(params, outputs, is_classification)
    """ Define model according to class. Set of default metrics are included for visualization """
    # Use cross entropy for classification and mean squared error for regression
    if params['mdn']:
        model_f = JointMDN
    elif not is_mdn and is_classification:
        model_f, loss_f = JointClassifier, CrossEntropy
    else:
        model_f, loss_f = JointRegressor, MeanSquared
    
    # Define metrics
    if params['mdn'] or not is_classification:
        metrics = [me.AbsError(), me.MeanPredError(), me.MedianPredError(), me.Mean(), me.Std()]
    else:
        metrics = [me.Accuracy(), me.AccuracyMode(outputs), me.AccuracyRandom(outputs)]
    
    # Build model arguments
    models = linear_model + mlp_model + cnn_model
    args = {'model_dir': logs_path,
            'models': models,
            'l1_reg': read_reg(params['l1_log_exp']),
            'l2_reg': read_reg(params['l2_log_exp']),
            'clip_gradient': read_gradient_clip(params['gradient_log_clip'])}

    # Add extra arguments
    if is_mdn:
        args.update({'mixture': params['K']})
    else:
        args.update({'loss_fn': loss_f})

    # Build model class
    joint = model_f(**args)
    return joint, metrics


def read_linear(name, cols, classes, params):
    """ Builds a linear model from the parameters """
    logger.info('Using columns {} for linear model'.format(cols))
    return LinearModel(name,
                       columns_list=cols,
                       optimizer=params[RandomSearch.linear_field('optimizer')],
                       initial_lr=read_lr(params[RandomSearch.linear_field('lr_log_exp')]),
                       decay_steps=params[RandomSearch.linear_field('lr_decay_steps')],
                       decay_rate=params[RandomSearch.linear_field('lr_decay_rate')])


def read_mlp(name, cols, classes, params):
    """ Builds a MLP from the parameters. Generates a MLP configuration to describe the network,
    which is stored in a temporary file
    Returns:
        model: MLP built
        fd: Descriptor of the network configuration
        path: Path of the temporary file containing the network configuration
    """
    # Store mlp definition in a temporary path
    fd, path = tempfile.mkstemp(suffix='.config')
    mlp_conf = create_mlp(layers=int(params['mlp_hidden_layers']),
                          hiddens=int(read_hidden(params['mlp_hidden_log_exp'])),
                          dropout=params['mlp_dropout'],
                          batch_norm=params['batch_norm'])
    mlp_conf.write(path)

    logger.info('Using columns {} for MLP'.format(cols))

    # Build model
    mlp_model = MLP(name,
                    columns_list=cols,
                    layers=path,
                    optimizer=params[RandomSearch.mlp_field('optimizer')],
                    initial_lr=read_lr(params[RandomSearch.mlp_field('lr_log_exp')]),
                    decay_steps=params[RandomSearch.mlp_field('lr_decay_steps')],
                    decay_rate=params[RandomSearch.mlp_field('lr_decay_rate')])

    return mlp_model, fd, path


def read_cnn(name, cols, classes, cnn_net, params):
    """ Builds a CNN from the given parameters """
    # Read pretrained weights path
    if params[RandomSearch.cnn_field('weights')] is None:
        cnn_weights = None
    else:
        cnn_weights = ut.get_model_weights(network=cnn_net,
                                           weights=params[RandomSearch.cnn_field('weights')])

    # Store temporary CNN config
    fd, path = tempfile.mkstemp(suffix='.config')
    cnn_conf = create_alexnet(dropout=params['cnn_dropout'])
    cnn_conf.write(path)

    # Create model out of
    cnn_model = CNNModel(name,
                         columns_list=cols,
                         cnn_layers=ut.get_network_definition(cnn_net),
                         optimizer=params[RandomSearch.cnn_field('optimizer')],
                         initial_lr=read_lr(params[RandomSearch.cnn_field('lr_log_exp')]),
                         decay_steps=params[RandomSearch.cnn_field('lr_decay_steps')],
                         decay_rate=params[RandomSearch.cnn_field('lr_decay_rate')],
                         weights=cnn_weights,
                         excluded_layers=['fc8'])  # excluded_layers will be ignored if no weights)

    return cnn_model, fd, path


def read_lr(lr_exp):
    """ Reads the learning rate for the model from the input exponent as 10^lr_exp """
    return float(np.power(10, lr_exp))


def read_embeddings(params):
    """ Reads embedding dimensions for the model from the parameters """
    return int(np.power(2, params['embeddings']))


def read_reg(value):
    """ Reads regularization as the power of 10 of the input exponent e.g. 10^value """
    return float(np.power(10, value)) if value is not None else None


def read_hidden(value):
    """ Returns hidden units read as a power of ten of the input exponent """
    return int(np.power(10, value))


def read_gradient_clip(value):
    """ Returns the value of the norm gradient clipping as a power of two of the input exponent """
    return int(np.power(2, value) if value is not None else None


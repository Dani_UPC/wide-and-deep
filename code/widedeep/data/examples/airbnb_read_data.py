"""

Reads the Proto files containing the Airbnb dataset. Expects several files to be contained in the
directory, identified by the set of data they belong to and the slice of data they represent.

Note that images are returned in BGR format since the PlacesCNN365 has been trained using that channel distribution.
Mean has also been subtracted using hardcoded values.

For a summary of the columns see airbnb_build_data.py.

"""

from widedeep.data.data_ops import DataMode
from widedeep.data.reading_ops import DataReader
from widedeep.data.airbnb import AirbnbSettings
from widedeep.data.image_ops import get_alexnet_specs
from widedeep.utils import Datasets, get_logger, get_dataset_location
import scipy

import tensorflow as tf

logger = get_logger('data')

tf.app.flags.DEFINE_string('batch_size', 1, 'Number of instances per batch')
tf.app.flags.DEFINE_bool('use_price', False, 'Whether to predict price (True) or availability (False)')
tf.app.flags.DEFINE_float('classification', True, 'Whether to build classification or regression dataset')
tf.app.flags.DEFINE_integer('memory_factor', 2,
                            'Factor related to the capacity of the queue (~GB). The '
                            + ' higher this amount, the more mixed the data but the slower '
                            + 'the processing time as well ')
tf.app.flags.DEFINE_integer('reader_threads', 4,
                            'Number of threads to read the instances.')

FLAGS = tf.app.flags.FLAGS


if __name__ == '__main__':

    with tf.Session() as sess:

        # Airbnb settings
        data = Datasets.AIRBNB_PRICE if FLAGS.use_price else Datasets.AIRBNB_AVAILABLE
        dataset = AirbnbSettings(dataset_location=get_dataset_location(data),
                                 image_specs=get_alexnet_specs(FLAGS.batch_size, random_crop=True))

        # Read batches from dataset
        reader = DataReader(dataset)
        features, label = reader.read_batch(batch_size=FLAGS.batch_size,
                                            data_mode=DataMode.TRAINING, # Use whatever here, e.g. training
                                            memory_factor=FLAGS.memory_factor,
                                            reader_threads=FLAGS.reader_threads,
                                            train_mode=False)

        # Initi all vars
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        # Define coordinator to handle all threads
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord, sess=sess)

        example, l = sess.run([features, label])

        # Print first instance in batch
        print(example)
        # Save image to check it is correct
        scipy.misc.imsave('test.jpeg', example['image'][0])
        # Show target for instance
        print 'Target: ' + str(l)

        coord.request_stop()
        coord.join(threads)

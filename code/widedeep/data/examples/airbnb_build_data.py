"""

Serialization of the Airbnb dataset into Example protos.

 The original data is converted into subdirectories that contain a moderate amount of files
 for both training and evaluation. Each file contains several Example Protos

  Columns contained in each Example:

    - accommodates: Number of rooms offered by the host.
    - area: City or city area where the listing comes from.
    - bathrooms: Number of bathrooms for the hosted offer.
    - bed_type: Type of bed.
    - bedrooms: Number of bedrooms of the offer.
    - cancelation_policy: Type of cancellation policy.
    - cleaning_fee: Cleaning fee price.
    - country: Country the listing belongs to.
    - final_price: Final price per night of the listing.
    - guests_included: Number of guests of the listing.
    - host_has_profile_pic: Whether the host has profile picture.
    - host_identify_verified: Whether the host identity has been verified by the system's authority.
    - host_is_superhost: Whether the host is recognized as superhost by Airbnb.
    - host_verifications: Number of social media verifications of the host.
    - instant_bookable: Whether the listing can be booked without previous host authorization.
    - minimum_nights: Minimum number of nights.
    - property_type: Type of property.
    - recent_review: Whether the listing has had any review in the last 30 days.
    - reviews_per_month: Monthly average of reviews.
    - review_scores_accuracy: Accuracy mean review score. Categories: non-rated, bad, normal, good.
    - review_scores_checkin: Check-in mean review score. Categories: non-rated, bad, normal, good.
    - review_scores_cleanliness: Cleanliness mean review score. Categories: non-rated, bad, normal, good.
    - review_scores_communication: Communication mean review score. Categories: non-rated, bad, normal, good.
    - review_scores_location: Location mean review score. Categories: non-rated, bad, normal, good.
    - review_scores_location: Value mean review score. Categories: non-rated, bad, normal, good.
    - review_scores_rating: Mean overall rating. Categories: non-rated, bad, normal, good.
    - room_type: Type of room offered.
    - sec_deposit: Amount of the security diposit.
    - state: State the listing belongs to.
    - subarea: Area/Neighbourhood the listing is located in.
    - amenities: Set of amenities/services that are offered by the host (e.g. wi-fi, dryer)
    - image: Cover image of the listing.

Some additonal metadata columns have been included but are not used for training, such as the identifier
of the listing, the time it was scraped or some metadata of the image (e.g. dimensions, url, format).

This has been coded following the ImageNet example in
https://github.com/tensorflow/models/blob/master/inception/README.md#getting-started

This class generates multiple files containing Example Protos of the Airbnb dataset. A separate
thread processes a different part of the data and divide it in shards. The total number of shards
is set beforehand and all threads generate approximately the same number of shards.

# TODO MODIFY
Using 256 shards for training and 32 for validation (and assuming 90% of the data goes for training)
, we obtain shards of ~15MB for training and ~12MB for validation.
It took ~30 minutes in a computer using 8 threads in Linux with i5 6600 processor and a
Asus GEFORCE 1060 with 6GB. We used the optimized protobuf library in here:
https://github.com/tensorflow/tensorflow/blob/master/tensorflow/g3doc/get_started/os_setup.md#protobuf-library-related-issues

"""

import widedeep.utils as ut
from widedeep.data.serialization_ops import DataSerializer
from widedeep.data.airbnb import AirbnbSerialize

import tensorflow as tf

logger = ut.get_logger('data')

# Data paths
tf.app.flags.DEFINE_string('data_path', ut.AIRBNB_FINAL, 'Airbnb pandas dataset')
tf.app.flags.DEFINE_string('amenities_path', ut.AIRBNB_METADATA, 'AIRBNB metadata path')

# Data parameters
tf.app.flags.DEFINE_integer('nq', None, 'Number of quantiles for numeric data normalization. Not used for zscores')
tf.app.flags.DEFINE_integer('subset', None, 'Number of instances to use. Set to None for all')
tf.app.flags.DEFINE_bool('use_price', True, 'Whether to predict price (True) or availability (False)')
tf.app.flags.DEFINE_float('train_ratio', 0.80, 'Ratio of training instances')
tf.app.flags.DEFINE_float('val_ratio', 0.10, 'Ratio of validation instances')

# Serialization parameters
tf.app.flags.DEFINE_integer('train_shards', 64, 'Number of output files in training TFRecord files.')
tf.app.flags.DEFINE_integer('validation_shards', 8, 'Number of output files in validation TFRecord files.')
tf.app.flags.DEFINE_integer('test_shards', 8, 'Number of output files in testing TFRecord files.')
tf.app.flags.DEFINE_integer('num_threads', 4, 'Number of threads to preprocess the images.')

FLAGS = tf.app.flags.FLAGS


if __name__ == '__main__':

    # Configuration for extraction
    settings = AirbnbSerialize(data_path=FLAGS.data_path,
                               amenities_path=FLAGS.amenities_path,
                               nq=FLAGS.nq,
                               subset=FLAGS.subset,
                               predict_price=FLAGS.use_price)

    # Save to TFRecord
    serializer = DataSerializer(settings)

    # Serialize data
    dataset = ut.Datasets.AIRBNB_PRICE if FLAGS.use_price else ut.Datasets.AIRBNB_AVAILABLE
    serializer.serialize(output_folder=ut.get_dataset_location(dataset),
                         train_ratio=FLAGS.train_ratio,
                         val_ratio=FLAGS.val_ratio,
                         num_threads=FLAGS.num_threads,
                         train_shards=FLAGS.train_shards,
                         val_shards=FLAGS.validation_shards,
                         test_shards=FLAGS.test_shards)

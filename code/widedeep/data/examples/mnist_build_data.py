"""

Serialization of the MNIST dataset into Example protos.

Creates a categorical column for each pixel in the image (784 columns) plus the usual
image and label columns

"""

import widedeep.utils as ut
from widedeep.data.serialization_ops import DataSerializer
from widedeep.data.mnist import MnistSerialize

import tensorflow as tf

# Dataset settings
tf.app.flags.DEFINE_float('train_ratio', 0.80,
                          'Ratio of training instances. The ratio is applied to rooms')
tf.app.flags.DEFINE_float('val_ratio', 0.10,
                          'Ratio of validation instances. The ratio is applied to rooms')
tf.app.flags.DEFINE_integer('train_shards', 80,
                            'Number of shards (output files) in training TFRecord files.')
tf.app.flags.DEFINE_integer('validation_shards', 8,
                            'Number of shards (output files) in validation TFRecord files.')
tf.app.flags.DEFINE_integer('test_shards', 8,
                            'Number of shards (output files) in testing TFRecord files.')
tf.app.flags.DEFINE_integer('num_threads', 8,
                            'Number of threads to preprocess the images.')

FLAGS = tf.app.flags.FLAGS


if __name__ == '__main__':

    # Configuration for extraction
    settings = MnistSerialize(data_path=ut.MNIST_DATA)
    # Save to TFRecord
    serializer = DataSerializer(settings)
    serializer.serialize(output_folder=ut.get_dataset_location(ut.Datasets.MNIST),
                         train_ratio=FLAGS.train_ratio, # Training ratio is not used
                         val_ratio=FLAGS.val_ratio, # Validation ratio is used
                         num_threads=FLAGS.num_threads,
                         train_shards=FLAGS.train_shards,
                         val_shards=FLAGS.validation_shards,
                         test_shards=FLAGS.test_shards)
